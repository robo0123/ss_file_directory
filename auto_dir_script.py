import os
import time
from openpyxl import Workbook, load_workbook
from openpyxl.styles import Font
from docx.api import Document
import logging

# 5:30 work

print('Program Start')
start = time.time()

logging.basicConfig(format='%(name)-8s %(asctime)s - %(levelname)-8s %(message)s',
                    filename='auto_dir_script.log',
                    encoding='utf-8', level=logging.NOTSET)

logging.info('*=======================Program Started=======================*')
logging.info('')


num_of_files = 0
entries = []

# FIND DOCUMENT AND KNOW ITS LOCATION
# loop through files, but find docx files
for dirpath, subdirs, files in os.walk("."):
    for filename in files:
        # IF IT'S A .docx FILE THEN DO THIS
        if filename.endswith('.docx'):
            # take note of file location and assign to document object
            file_location = os.path.join(dirpath, filename)
            document = Document(file_location)
            num_of_files += 1
            logging.info('Scanning ' + filename +
                         ' located at ' + file_location)
            # initialize table to gather data
            table = None
            try:
                # get first table
                table = document.tables[0]
                logging.info(filename + ': table found ')
            except IndexError:
                # if there are no tables
                logging.error('IndexError: There might be no table with file ' +
                              filename + ' located in ' + file_location)
                continue

            # get data from table
            data_in_table = []
            for i, row in enumerate(table.rows):
                text_generator = (cell.text for cell in row.cells)
                for word in text_generator:
                    data_in_table.append(word)

            # keywords to find necessary data in table
            keywords = ['document number', 'date', 'to']
            # initialize a dict to put later in entries list
            an_entry_dict = {}
            # make dict with values
            for word in data_in_table:
                if word.lower() in keywords:
                    kw_pos = data_in_table.index(word)
                    kw_val = data_in_table[kw_pos + 1]
                    an_entry_dict[word.lower()] = kw_val
                    keywords.remove(word.lower())

            # add the dict to 'entries' list variable
            # if the dict has missing a key, it doesn't add it to 'entries'
            s1 = set(['document number', 'date', 'to'])
            if s1.issubset(an_entry_dict.keys()):
                an_entry_dict['location'] = file_location
                entries.append(an_entry_dict)
                logging.info(filename + ': Successfully added to entries.')
            else:
                logging.error(
                    filename + ': Document Number, Date, or To, not found in table.')

# get or create xlsx file
wb_name = 'directory_workbook.xlsx'
wb = None
try:
    # get workbook
    wb = load_workbook(filename=wb_name)
    logging.info(wb_name + ' opened.')
except FileNotFoundError:
    # create workbook
    logging.error('FileNotFoundError: ' + wb_name + ' not found.')
    wb = Workbook()
    wb.save(wb_name)
    logging.info(wb_name + ' created.')

# remove worksheets
for sheet in wb.sheetnames:
    wb.remove(wb[sheet])
logging.info(wb_name + ': Sheets removed.')

# create directory sheet
wb.create_sheet('directory')
logging.info('Worksheet "directory" created.')

# access sheet named 'directory'
ws = wb['directory']
logging.info('Worksheet "directory" accessed.')

# add column names
ws["A1"] = "Document Code"
ws["B1"] = "Name Of Recipient"
ws["C1"] = "Date Issued"
ws["D1"] = "File Location"
logging.info("Column names created.")

# add entries
for entry in entries:
    temp_list = [entry['document number'],
                 entry['to'], entry['date'], entry['location']]
    ws.append(temp_list)
logging.info("Entries added.")

# add entry count
ws["G1"] = "Number of Entries"
ws["G2"] = len(entries)
logging.info("Total number of entries is: " + str(len(entries)))

# set style
bold_font = Font(bold=True)
col_title = [ws["A1"], ws["B1"], ws["C1"], ws["D1"], ws["G1"]]
for title in col_title:
    title.font = bold_font
logging.info("Font styles set.")

# set filters and sorts
# ws.auto_filter.add_sort_condition("C2:C" + str(ws.max_row))

# save workbook
wb.save(filename=wb_name)
logging.info("Workbook saved.")

end = time.time()

logging.info('')
logging.info(f"Runtime of the program is {end - start}")
logging.info('*=======================Program End=======================*')

print('Program End')
print(f"Runtime of the program is {end - start}")
